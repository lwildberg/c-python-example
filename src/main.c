
#include <glib.h>
#include <stdlib.h>
#include <gobject-introspection-1.0/girepository.h>
#include <pygobject-3.0/pygobject.h>
#include "resources.h"


gint
main (gint   argc,
      gchar *argv[])
{
  GResource *resource = gresource_get_resource ();
  
  GError *error = NULL;
  GBytes *typelib_data = g_resource_lookup_data (resource,
                                                 "/org/gnome/gitlab/lwildberg/c-python-example/Example-1.typelib",
                                                 G_RESOURCE_LOOKUP_FLAGS_NONE,
                                                 &error
                                                 );
  if (error != NULL || typelib_data == NULL) {
    printf ("error looking up typelib resource: %s\n", error->message);
    return EXIT_FAILURE;
  }
  
  GITypelib *typelib = g_typelib_new_from_const_memory ((const guint8 *) g_bytes_get_data (typelib_data, NULL),
                                                  g_bytes_get_size (typelib_data),
                                                  &error
                                                  );
  if (error != NULL) {
    printf ("error loading typelib resource: %s\n", error->message);
    return EXIT_FAILURE;
  }
  
  g_irepository_load_typelib (NULL,
                              typelib,
                              0,
                              &error
                              );
  if (error != NULL) {
    printf ("error loading typelib: %s\n", error->message);
    return EXIT_FAILURE;
  }
  
  Py_Initialize ();
  PyObject *object = pygobject_init (-1, -1, -1);
  if (object == NULL) {
    printf("pygobject_init failed\n");
    return EXIT_FAILURE;
  }
  
  GBytes *py_data = g_resource_lookup_data (resource,
                                            "/org/gnome/gitlab/lwildberg/c-python-example/main.py",
                                            G_RESOURCE_LOOKUP_FLAGS_NONE,
                                            &error);
  if (error != NULL) {
    printf ("error loading python typelib: %s\n", error->message);
    return EXIT_FAILURE;
  }
  
  PyObject *main_py_code = Py_CompileString ((gchar *) g_bytes_get_data (py_data, NULL),
                                               "main",
                                               Py_file_input
                                               );
  PyObject *main_py_module = PyImport_ExecCodeModule ("main", main_py_code);
  PyObject *main_func = PyObject_GetAttrString (main_py_module, "main");
  GClosure *main_func_closure = _PyGObject_API->closure_new (main_func, NULL, NULL);
  g_closure_invoke (main_func_closure, NULL, 0, NULL, NULL);

  return EXIT_SUCCESS;
}
