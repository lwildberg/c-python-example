#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define EXAMPLE_TYPE_OBJECT (example_object_get_type())

G_DECLARE_FINAL_TYPE (ExampleObject, example_object, EXAMPLE, OBJECT, GObject)

ExampleObject *example_object_new (void);

void example_object_print_custom (ExampleObject *self);

G_END_DECLS
