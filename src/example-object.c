#include "example-object.h"

struct _ExampleObject
{
  GObject parent_instance;
};

G_DEFINE_FINAL_TYPE (ExampleObject, example_object, G_TYPE_OBJECT)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

ExampleObject *
example_object_new (void)
{
  return g_object_new (EXAMPLE_TYPE_OBJECT, NULL);
}

void
example_object_print_custom (ExampleObject *self)
{
  printf ("hi, this is C print_custom ()\n");
}

static void
example_object_finalize (GObject *object)
{
  ExampleObject *self = (ExampleObject *)object;

  G_OBJECT_CLASS (example_object_parent_class)->finalize (object);
}

static void
example_object_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  ExampleObject *self = EXAMPLE_OBJECT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
example_object_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  ExampleObject *self = EXAMPLE_OBJECT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
example_object_class_init (ExampleObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = example_object_finalize;
  object_class->get_property = example_object_get_property;
  object_class->set_property = example_object_set_property;
}

static void
example_object_init (ExampleObject *self)
{
}
